package main;

import java.util.Scanner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import util.DataBaseUtil;

public class Main extends Application {
	/**
	 * loading our fxml and sceneBuilder code
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			primaryStage.setTitle("Medical Clinic");
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene = new Scene(root, 600, 600);
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		launch(args);
	}
}
