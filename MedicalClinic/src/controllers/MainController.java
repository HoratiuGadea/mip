package controllers;

import java.net.URL;
import java.util.List;
import java.util.Observable;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import util.DataBaseUtil;
import model.Patient;


public class MainController implements Initializable {

	@FXML
	private ListView<String> listView;
	
	@FXML
    private TextField username;
	
    @FXML
    private PasswordField password;
    /**
     * what will occur when pressing "log in" button
     * @param event
     * @throws Exception
     */
    @FXML
    void login(ActionEvent event) throws Exception {
    	DataBaseUtil db = new DataBaseUtil();
        db.setUp();
        db.startTransaction();
        db.login(username, password);
        db.commitTransaction();
        db.stopEntityManager();
    }
	
    @FXML
    private ImageView imageView;
    
	public void populateMainListView() throws Exception {
		DataBaseUtil db = new DataBaseUtil();
		db.setUp();
		db.startTransaction();
		List<Patient> patientDBList = (List <Patient>) db.patientList();
		ObservableList<String> patientNamesList = getPatientName(patientDBList);
		db.stopEntityManager();
	}
	/**
	 * trying to fit in ObservableList, no result
	 * @param patients - list of patients
	 * @return	names of the pacients
	 */
	public ObservableList<String> getPatientName (List<Patient> patients) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for(Patient p: patients) {
			names.add(p.getName());
		}
		return names;
	}
	
	/**
	 * initializing fade transition for our .jpg 
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			Image image = new Image("/images/clinic.jpg");
	        imageView.setImage(image);
	        FadeTransition ft = new FadeTransition(Duration.seconds(5), imageView);
	        ft.setFromValue(0.0);
	        ft.setToValue(1.0);
	        ft.play();
			populateMainListView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
