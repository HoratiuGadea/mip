package util;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.Appointment;
import model.Medicalpersonal;
import model.Patient;

public class DataBaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	Scanner in = new Scanner(System.in);
	
	public void setUp()throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("MedicalClinic");
		entityManager=entityManagerFactory.createEntityManager();
	}
	public void startTransaction(){
		entityManager.getTransaction().begin();
	}
	public void commitTransaction(){
		entityManager.getTransaction().commit();
	}
	public void stopEntityManager(){
		entityManager.close();
	}
	/**
	 * entering a new Pacient with his informations
	 */
	public void newPatient()
	{
		Patient patient = new Patient();
		System.out.println("Enter your ID:");
		patient.setIdPatient(in.nextInt());
		System.out.println("Enter your name:");
		patient.setName(in.next());
		System.out.println("Enter your age:");
		patient.setAge(in.nextInt());
		System.out.println("Enter yout bloodType:");
		patient.setBloodType(in.next());
		System.out.println("Let's make an appointment!");
		System.out.println("");
		patient.addAppointment(addNewAppointment());
		entityManager.persist(patient);
	}
	/**
	 * add/delete apoointments for already registered patients
	 * @param id - pacient id
	 */
	public void existingPatient(int id)
	{
		Patient patient=entityManager.find(Patient.class,id);
		System.out.println("1.Add new appointment");
		System.out.println("2.Delete appointment");
		int ch=in.nextInt();
		switch (ch) 
		{
		case 1:patient.addAppointment(addNewAppointment());break;
		case 2:{
				System.out.println("Enter the id of the appointment:");
				patient.removeAppointment(in.nextInt());
				}
		}
	}
	/**
	 * add new appointment
	 * @return appointment - new appointment
	 */
	public Appointment addNewAppointment()
	{
		Appointment appointment= new Appointment();
		Date dateAndTime = new Date();
		System.out.println("Enter the ID of your appointment:");
		appointment.setIdAppointment(in.nextInt());
		System.out.println("Enter the day:");
		dateAndTime.setDate(in.nextInt());
		System.out.println("Enter the month:");
		dateAndTime.setMonth(in.nextInt());
		System.out.println("Enter the year:");
		dateAndTime.setYear(in.nextInt());
		System.out.println("Enter the hour:");
		dateAndTime.setHours(in.nextInt());
		System.out.println("Enter the minute:");
		dateAndTime.setMinutes(in.nextInt());
		
		appointment.setDateAndTime(dateAndTime);
		System.out.println("Enter the reason:");
		appointment.setReason(in.next());
		System.out.println("Choose the ID of the doctor:");
		printAllMedicalPersonal();
		Medicalpersonal aux=entityManager.find(Medicalpersonal.class,in.nextInt());
		appointment.setMedicalpersonal(aux);
		entityManager.persist(appointment);
		
		return appointment;
	}
	/**
	 * listing all pacients that have appointments
	 */
	public void printAllPatientsWithAppointments()
	{
		
		List<Patient> allPatients = entityManager.createNativeQuery("Select * from medicalclinic.patient", Patient.class).getResultList();
		for(Patient index:allPatients)
		{
			System.out.println(index.getIdPatient()+". "+index.getName()+" has "+index.getAge()+" years old with bloodType:"+index.getBloodType()+" has appointments: ");
			List<Appointment> appointments=index.getAppointments();
			for(Appointment app:appointments)
				System.out.println(app.getIdAppointment()+". "+app.getDateAndTime()+" "+app.getReason()+" at the doctor: "+app.getMedicalpersonal().getName()+" - "+app.getMedicalpersonal().getSpecialization());
		}
	}
	/**
	 * entering a new MedicalPersonal with his informations
	 */
	public void createMedicalPersonal()
	{
		Medicalpersonal medic = new Medicalpersonal();
		System.out.println("Enter ID:");
		medic.setIdMedicalPerosonal(in.nextInt());		
		System.out.println("Enter the name:");
		medic.setName(in.next());
		System.out.println("Enter the specialization:");
		medic.setSpecialization(in.next());
		entityManager.persist(medic);
	}
	/**
	 * listing all doctors with their specialization
	 */
	public void printAllMedicalPersonal()
	{
		List<Medicalpersonal> doctors = entityManager.createNativeQuery("Select * from medicalclinic.medicalperosonal", Medicalpersonal.class).getResultList();
		for(Medicalpersonal index:doctors)
		{
			System.out.println(index.getIdMedicalPerosonal()+ ". "+ index.getName()+" has specialization: "+index.getSpecialization());
		}
	}
	
	public List<Patient> patientList(){
		List<Patient> patientList = (List<Patient>)entityManager.createQuery("SELECT p FROM Patient p",Patient.class).getResultList();
		return patientList;
	}
	/**
	 * verifying registered accounts
	 * @param username - username for registered account
	 * @param password - password for registered account
	 * @return
	 */
	public boolean login(TextField username, PasswordField password) {
        List<Patient> patient = entityManager.createNativeQuery("SELECT * FROM medicalclinic.patient;", Patient.class).getResultList();

        for (Patient index : patient) {
            if (index.getName().equals(username.getText())) {
                if (index.getPassword().equals(password.getText())) {
                    System.out.println("Authentification successful\n\n");
                    return true;
                }
            }
        }
        System.out.println("Authentification failed!\nPlease re-enter your username and password!\n\n");
        return false;
    }
	/**
	 * listing all appointments for a registered patient
	 * @param username - username for registered account
	 * @param password - password for registered account
	 */
	public void appointments(TextField username, PasswordField password) {
        List<Appointment> appointments = entityManager.createNativeQuery("SELECT * FROM medicalclinic.appointment;", Appointment.class).getResultList();

        for (Appointment index : appointments) {
            if (login(username,password)==true) {
            	System.out.println(username.getText()+ " you have appointment(s): ");
            }
        }
}
}