package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the departments database table.
 * 
 */
@Entity
@Table(name="departments")
@NamedQuery(name="Department.findAll", query="SELECT d FROM Department d")
public class Department implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idDepartments;

	private String name;

	private String primaryMedic;

	public Department() {
	}

	public int getIdDepartments() {
		return this.idDepartments;
	}

	public void setIdDepartments(int idDepartments) {
		this.idDepartments = idDepartments;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrimaryMedic() {
		return this.primaryMedic;
	}

	public void setPrimaryMedic(String primaryMedic) {
		this.primaryMedic = primaryMedic;
	}

}