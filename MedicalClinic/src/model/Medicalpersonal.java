package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medicalpersonal database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalpersonal.findAll", query="SELECT m FROM Medicalpersonal m")
public class Medicalpersonal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idMedicalPerosonal;

	private int age;

	private String cnp;

	private String name;

	private String password;

	private String specialization;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="medicalpersonal")
	private List<Appointment> appointments;

	public Medicalpersonal() {
	}

	public int getIdMedicalPerosonal() {
		return this.idMedicalPerosonal;
	}

	public void setIdMedicalPerosonal(int idMedicalPerosonal) {
		this.idMedicalPerosonal = idMedicalPerosonal;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCnp() {
		return this.cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}
	/**
	 * adding an appointment for a doctor
	 * @param appointment - new appointment
	 * @return appointment
	 */
	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setMedicalpersonal(this);

		return appointment;
	}
	/**
	 * removing an appointment from a doctor
	 * @param appointment - existing appointment
	 * @return appointment
	 */
	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setMedicalpersonal(null);

		return appointment;
	}

}