package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the medicalrecords database table.
 * 
 */
@Entity
@Table(name="medicalrecords")
@NamedQuery(name="Medicalrecord.findAll", query="SELECT m FROM Medicalrecord m")
public class Medicalrecord implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idMedicalRecords;

	@Temporal(TemporalType.DATE)
	private Date dateIN;

	@Temporal(TemporalType.DATE)
	private Date dateOUT;

	private int fk_idDepartment;

	private int fk_idMedicalPersonal;

	private int fk_idPatient;

	private String history;

	public Medicalrecord() {
	}

	public int getIdMedicalRecords() {
		return this.idMedicalRecords;
	}

	public void setIdMedicalRecords(int idMedicalRecords) {
		this.idMedicalRecords = idMedicalRecords;
	}

	public Date getDateIN() {
		return this.dateIN;
	}

	public void setDateIN(Date dateIN) {
		this.dateIN = dateIN;
	}

	public Date getDateOUT() {
		return this.dateOUT;
	}

	public void setDateOUT(Date dateOUT) {
		this.dateOUT = dateOUT;
	}

	public int getFk_idDepartment() {
		return this.fk_idDepartment;
	}

	public void setFk_idDepartment(int fk_idDepartment) {
		this.fk_idDepartment = fk_idDepartment;
	}

	public int getFk_idMedicalPersonal() {
		return this.fk_idMedicalPersonal;
	}

	public void setFk_idMedicalPersonal(int fk_idMedicalPersonal) {
		this.fk_idMedicalPersonal = fk_idMedicalPersonal;
	}

	public int getFk_idPatient() {
		return this.fk_idPatient;
	}

	public void setFk_idPatient(int fk_idPatient) {
		this.fk_idPatient = fk_idPatient;
	}

	public String getHistory() {
		return this.history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

}