package util;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Appointment;
import model.Medicalperosonal;
import model.Patient;

public class DataBaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	Scanner in = new Scanner(System.in);
	
	public void setUp()throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("MedicalClinicJPA");
		entityManager=entityManagerFactory.createEntityManager();
	}
	public void startTransaction(){
		entityManager.getTransaction().begin();
	}
	public void commitTransaction(){
		entityManager.getTransaction().commit();
	}
	public void stopEntityManager(){
		entityManager.close();
	}
	public void newPatient()
	{
		Patient patient = new Patient();
		System.out.println("Enter your ID:");
		patient.setIdPatient(in.nextInt());
		System.out.println("Enter your name:");
		patient.setName(in.next());
		System.out.println("Enter your age:");
		patient.setAge(in.nextInt());
		System.out.println("Enter your bloodType:");
		patient.setBloodType(in.next());
		System.out.println("Enter a password");
		patient.setPassword(in.next());
		System.out.println("Let's make an appointment!");
		System.out.println("");
		patient.addAppointment(addNewAppointment());
		entityManager.persist(patient);
	}

	public Appointment addNewAppointment()
	{
		Appointment appointment= new Appointment();
		Date dateAndTime = new Date();
		System.out.println("Enter the ID of your appointment:");
		appointment.setIdAppointment(in.nextInt());
		System.out.println("Enter the day:");
		dateAndTime.setDate(in.nextInt());
		System.out.println("Enter the month:");
		dateAndTime.setMonth(in.nextInt());
		System.out.println("Enter the year:");
		dateAndTime.setYear(in.nextInt());
		System.out.println("Enter the hour:");
		dateAndTime.setHours(in.nextInt());
		System.out.println("Enter the minute:");
		dateAndTime.setMinutes(in.nextInt());
		
		appointment.setDateAndTime(dateAndTime);
		System.out.println("Enter the reason:");
		appointment.setReason(in.next());
		System.out.println("Choose the ID of the doctor:");
		printAllMedicalPersonal();
		Medicalperosonal aux=entityManager.find(Medicalperosonal.class,in.nextInt());
		appointment.setMedicalperosonal(aux);
		entityManager.persist(appointment);
		
		return appointment;
	}
	public void printAllPatientsWithAppointments()
	{
		
		List<Patient> allPatients = entityManager.createNativeQuery("Select * from medicalclinic.patient", Patient.class).getResultList();
		for(Patient index:allPatients)
		{
			System.out.println(index.getIdPatient()+". "+index.getName()+" has "+index.getAge()+" years old with bloodType:"+index.getBloodType()+" has appointments: ");
			List<Appointment> appointments=index.getAppointments();
			for(Appointment app:appointments)
				System.out.println(app.getIdAppointment()+". "+app.getDateAndTime()+" "+app.getReason()+" at the doctor: "+app.getMedicalperosonal().getName()+" - "+app.getMedicalperosonal().getSpecialization());
		}
	}
	public void createMedicalPersonal()
	{
		Medicalperosonal medic = new Medicalperosonal();
		System.out.println("Enter ID:");
		medic.setIdMedicalPerosonal(in.nextInt());		
		System.out.println("Enter the name:");
		medic.setName(in.next());
		System.out.println("Enter the specialization:");
		medic.setSpecialization(in.next());
		entityManager.persist(medic);
	}
	public void printAllMedicalPersonal()
	{
		List<Medicalperosonal> doctors = entityManager.createNativeQuery("Select * from medicalclinic.medicalperosonal", Medicalperosonal.class).getResultList();
		for(Medicalperosonal index:doctors)
		{
			System.out.println(index.getIdMedicalPerosonal()+ ". "+ index.getName()+" has specialization: "+index.getSpecialization());
		}
	}
	
	public List<Patient> patientList(){
		List<Patient> patientList = (List<Patient>)entityManager.createQuery("SELECT p FROM Patient p",Patient.class).getResultList();
		return patientList;
	}
	public List<Medicalperosonal> medicalPersonalList(){
		List<Medicalperosonal> medicalPersonalList = (List<Medicalperosonal>)entityManager.createQuery("SELECT m FROM Medicalperosonal m",Medicalperosonal.class).getResultList();
		return medicalPersonalList;
	}
	public List<Appointment> appointmentList(String nameMedic){
		List<Appointment> appointmentList = (List<Appointment>)entityManager.createQuery("SELECT a FROM Appointment a",Appointment.class).getResultList();
		for(Appointment index:appointmentList)
		{
			if(index.getMedicalperosonal().getName().equals(nameMedic)==false)
				appointmentList.remove(index);
		}
		return appointmentList;
	}
}
