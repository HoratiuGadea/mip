package controllers;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.sun.glass.ui.Menu;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import model.Appointment;
import model.Medicalperosonal;
import model.Patient;
import util.DataBaseUtil;

public class MainController implements Initializable{

	
	@FXML
	private ListView<String> listView;
	@FXML
	private ListView<Date> dateListView;
	@FXML
    private MenuItem newPatientButton;
    @FXML
    private MenuItem existingPatientButton;
    @FXML
    private TextField userName;
    @FXML
    private PasswordField password;
    @FXML
    private Button signInButton;
    @FXML
    private Button medicalPersonalButton;
    @FXML
    private HBox hBoxLogIn;
    
    @FXML
    void newPatientButton(ActionEvent event) throws Exception {
    	DataBaseUtil db = new DataBaseUtil();
    	db.setUp();
    	db.startTransaction();
    	
    }
    @FXML
    void existingPatientButton(ActionEvent event) throws Exception {
    }
    
    @FXML
    void medicalPersonalButton(ActionEvent event) {
    	hBoxLogIn.setVisible(true);
    }
    @FXML
    void signInButton(ActionEvent event) throws Exception {
    	DataBaseUtil db = new DataBaseUtil();
    	db.setUp();
    	db.startTransaction();
    	List<Medicalperosonal>medicalPersonalDBList=(List<Medicalperosonal>) db.medicalPersonalList();
    	for(Medicalperosonal index : medicalPersonalDBList)
    	{
    		if(index.getName().equals(userName.getText())){
    			if(index.getPassword().equals(password.getText())){
    					System.out.println(index.getName()+" "+index.getSpecialization());
    					hBoxLogIn.setVisible(false);
    					dateListView.setVisible(true);
    					populateAppointmentsList();
    					
    			}
    		}
    	}
    }
	public void populatemainListView() throws Exception {
		DataBaseUtil db = new DataBaseUtil();
		db.setUp();
		db.startTransaction();
		List<Patient>patientDBList = (List<Patient>) db.patientList();
		ObservableList<String> patientNamesList = getPatientName(patientDBList);
		listView.setItems(patientNamesList);
		listView.refresh();
		db.stopEntityManager();	
	}
	
	public void populateAppointmentsList() throws Exception{
			
		DataBaseUtil db = new DataBaseUtil();
		db.setUp();
		db.startTransaction();
		
		List<Appointment> appointmentsDBList = (List<Appointment>) db.appointmentList(userName.getText());
		ObservableList<Date> appointmentDateList = getAppointmentDate(appointmentsDBList);
		dateListView.setItems(appointmentDateList);
		dateListView.refresh();
		db.stopEntityManager();
	}
	public ObservableList<String> getPatientName (List<Patient> patients){
		ObservableList<String> names = FXCollections.observableArrayList();
		for(Patient index:patients)
			names.add(index.getName());
		
		return names;
	}
	
	public ObservableList<Date> getAppointmentDate (List<Appointment> appointments){
		ObservableList<Date> appointment = FXCollections.observableArrayList();
		for(Appointment index:appointments)
			appointment.add(index.getDateAndTime());
		
		return appointment;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
