package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the appointment database table.
 * 
 */
@Entity
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idAppointment;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateAndTime;

	private String reason;

	//bi-directional many-to-one association to Medicalperosonal
	@ManyToOne
	@JoinColumn(name="fk_medicalPersonal")
	private Medicalperosonal medicalperosonal;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name="fk_patient")
	private Patient patient;

	public Appointment() {
	}

	public int getIdAppointment() {
		return this.idAppointment;
	}

	public void setIdAppointment(int idAppointment) {
		this.idAppointment = idAppointment;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Medicalperosonal getMedicalperosonal() {
		return this.medicalperosonal;
	}

	public void setMedicalperosonal(Medicalperosonal medicalperosonal) {
		this.medicalperosonal = medicalperosonal;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}
}