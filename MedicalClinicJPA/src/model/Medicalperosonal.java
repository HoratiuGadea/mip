package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medicalperosonal database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalperosonal.findAll", query="SELECT m FROM Medicalperosonal m")
public class Medicalperosonal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idMedicalPerosonal;

	private String name;

	private String specialization;
	
	private String password;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="medicalperosonal")
	private List<Appointment> appointments;

	public Medicalperosonal() {
	}

	public int getIdMedicalPerosonal() {
		return this.idMedicalPerosonal;
	}

	public void setIdMedicalPerosonal(int idMedicalPerosonal) {
		this.idMedicalPerosonal = idMedicalPerosonal;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password){
		this.password=password;
	}
	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setMedicalperosonal(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setMedicalperosonal(null);

		return appointment;
	}

}